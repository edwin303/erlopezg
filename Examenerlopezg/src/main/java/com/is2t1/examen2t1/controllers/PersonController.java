/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.examen2t1.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import com.is2t1.exmaen2t1.models.Person;
import com.is2t1.examen2t1.viewa.EmployeeFrame;
import com.is2t1.examen2t1.misc.Util;

/**
 *
 * @author Sistemas-17
 */
public class PersonController implements ActionListener, FocusListener{
    
     EmployeeFrame EmployeeFrame;
    JComboBox dComboBox;
    JComboBox mComboBox;
    JFileChooser d;
    Person person;
    
     public PersonController(EmployeeFrame f) {
        super();
        EmployeeFrame = f;
        d = new JFileChooser();
        person = new Person();
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person b) {
        this.person = b;
    }
    
    public void setPerson(String filePath) {
        File f = new File(filePath);
        readPerson(f);
    }
    
    public void setjobComboBox(JComboBox jcombo) {
        mComboBox = jcombo;
    }
     
     

    @Override
    public void actionPerformed(ActionEvent e) {
                if(e.getSource().getClass().getName().equals(JComboBox.class.getName()))
        {
            JComboBox cbo = (JComboBox)e.getSource();
            switch(cbo.getName()){
                case "jLabel8":
                    switch(cbo.getSelectedItem().toString()) {
                        case "jLabel8":
                            mComboBox.setModel(new DefaultComboBoxModel());
                            break;
                        
                        default:
                            mComboBox.setModel(new DefaultComboBoxModel(new String[]{}));
                            break;
                    }
                    break;
            }
        } else {
            switch (e.getActionCommand()) {
                case "save":
                    d.showSaveDialog(EmployeeFrame);
                    person = EmployeeFrame.getPersonData();
                    writePerson(d.getSelectedFile());
                    break;
                case "select":
                    d.showOpenDialog(EmployeeFrame);
                    person = readPerson(d.getSelectedFile());
                    EmployeeFrame.setPersonData(person);
                    break;
                case "clear":
                    EmployeeFrame.clear();
                    break;

            }
        }

    }

    @Override
    public void focusGained(FocusEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void focusLost(FocusEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private Person readPerson(File file) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            return (Person) ois.readObject();
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(EmployeeFrame, e.getMessage(), EmployeeFrame.getTitle(), JOptionPane.WARNING_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(PersonController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private void writePerson(File file) {
        try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(getPerson());
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(PersonController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 
}
