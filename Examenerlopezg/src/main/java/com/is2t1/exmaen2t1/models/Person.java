/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.exmaen2t1.models;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Sistemas-17
 */
public class Person implements Serializable {
     private String id;
    private String Name;
    private String Name2;
    private String lastaName2;
    private String lastName;
    private Date birthDate;
    private int age;

    public String getName2() {
        return Name2;
    }

    public void setName2(String Name2) {
        this.Name2 = Name2;
    }

    public String getLastaName2() {
        return lastaName2;
    }

    public void setLastaName2(String lastaName2) {
        this.lastaName2 = lastaName2;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
